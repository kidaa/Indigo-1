import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.tools.*;

public class IBasicFeatures
{
	static PrintWriter clear = null;
	static PrintWriter p = null;
	
	static String className = null;
	static String output = "";
	
	public IBasicFeatures(String s)
	{
		className = s;
		
		try
		{
			clear = new PrintWriter(new File(className + ".java"));
			p = new PrintWriter(new FileWriter(new File(className + ".java"), true));
		}
		catch (IOException e)
		{
			System.out.println("Sorry, compiling could not be completed :(");
		}
	}
	
	public static void clearFile()
	{
		clear.println();
		clear.close();
	}
	
	public static void compileAndRun()
	{
		try
    	{  
    		String store = "";
    		
        	Process pro1 = Runtime.getRuntime().exec("javac " + className + ".java");  
            pro1.waitFor();  
            Process pro2 = Runtime.getRuntime().exec("java " + className);  
            
            InputStream istr = pro2.getInputStream(); 
           	BufferedReader br = new BufferedReader(new InputStreamReader(istr));
           	
    		String str = "";
    		while ( (str =  br.readLine()) != null )
        		System.out.println(str);
        		
        	pro2.waitFor(); 
        	
        	br.close();
        }
        catch (Exception e)
        {    
            System.out.println("Some Error");   
            e.printStackTrace();     
        }  
	}
	
	public static void methodCall(String line)
	{
		if (!line.contains(" send "))
			p.println(line.substring(3) + "();");
		else
		{
			p.print(line.substring(3, line.indexOf(" send ")));
					
			output += "(" + line.substring(line.indexOf(" send ") + 6) + ");";
			p.println(output);
							
			output = "";
		}
	}
	
	public static void writeClassHeader(String line)
	{
		output += "public class ";
				
		line = line.substring(9);
				
		if (line.contains(" "))
		{
			int index = line.indexOf(" ");
					
			String rest = line.substring(index + 1);
					
			line = line.substring(0, index);
					
			output += line + " ";
			output += rest;
		}
		else
			output += line;
				
		output += "\n{\n";
					
		p.println(output);
		
		output = "";
	}
	
	public static void writeMethodHeader(String line)
	{
		String temp = "";
		
		output += "public static void ";
		
		if (!line.contains(" receive "))
			output += line.substring(5) + "()";
		else
		{
			output += line.substring(5, line.indexOf(" receive "));
				
			temp = line.substring(line.indexOf(" receive ") + 9);
			String[] parts = temp.split("(?=,)");
			
			for (int i = 0; i < parts.length; i++)
			{
				String copy = parts[i];
			
				if ((parts[i].indexOf(" ") < parts[i].indexOf("num")) || (parts[i].indexOf(" ") < parts[i].indexOf("dec")) || (parts[i].indexOf(" ") < parts[i].indexOf("word")))
					parts[i] = parts[i].substring(0, parts[i].indexOf(" ")) + parts[i].substring(parts[i].indexOf(" ") + 1);
				
				String str1 = parts[i].substring(parts[i].indexOf(",") + 1, parts[i].indexOf(" "));
				String str2 = parts[i].substring(parts[i].indexOf(" "));
				
				str1 = str1.replace("num", "int");
				str1 = str1.replace("dec", "double");
				str1 = str1.replace("word", "String");
				
				if (copy.contains(","))
					str1 = ", " + str1;
				parts[i] = str1 + str2;
			}
			
			temp = "";
			
			for (int j = 0; j < parts.length; j++)
				temp += parts[j];
			
			output += "(" + temp + ")";
		}
				
		output += "\n{";
				
		p.println(output);
		
		output = "";
	}
	
	public static void print(String line)
	{
		p.println("System.out.print(" + line.substring(6) + ");");
	}
	
	public static void tryer(String line)
	{
		p.println("try\n{");
	}
	
	public static void catcher(String line)
	{
		p.println("}\ncatch (FileNotFoundException ex)\n{\nex.getStackTrace();\n}\ncatch (ArithmeticException ex)\n{\nex.getStackTrace();\n}\ncatch (NumberFormatException ex)\n{\nex.getStackTrace();\n}");
	}
	
	public static void forLoop(String line)
	{
		line = line.substring(7);
		
		line = line.replace("let", "int");
		line = line.replace("while", ";");
		if (line.contains("increment"))
		{
			line = line.replace("increment", ";");
			line = line.replace(" by ", "+=");
		}
		else
		{
			line = line.replace("decrement", ";");
			line = line.replace(" by ", "-=");
		}
		
		line = "for (" + line + ")\n{";
	
		p.println(line);
	}
	
	public static void image3(String line)
	{
		line = line.replace("drawImage", "drawImage(");
		line = line.replace("to", ",");
		line = line.replace("from", ",");
		line = line.replace("with buffer", ",");
		
		line += ");";
		p.println(line);
	}

	public static void image2(String line)
	{
		line = line.replace("drawImage", "drawImage(");
		line = line.replace("at", ",");
		line = line.replace("with", ",");
		line = line.replace("and", ",");
		line = line.replace("with buffer", ",");
		
		line += ");";
		p.println(line);
	}

	public static void image1(String line)
	{
		line = line.replace("drawImage", "drawImage(");
		line = line.replace("at", ",");
		line = line.replace("with buffer", ",");
		
		line += ");";
	}
	
	public static void line(String line)
	{
		p.println("System.out.println(" + line.substring(5) + ");");
	}
	
	public static void mainMethodHeader()
	{
		p.println("public static void main(String[] args)\n{");
	}
	
	public static void declareAndInitialize(String line)
	{
		String name = line.substring(4, line.indexOf("=")).trim();
		String value = line.substring(line.indexOf("=") + 1).trim();
			
		if (isInteger(value))
			p.println("int " + name + " = " + value + ";");
		else if (isDouble(value))
			p.println("double " + name + " = " + value + ";");
		else
			p.println("String " + name + " = " + value + ";");
	}
	
	public static void objectCreation(String line)
	{
		String category = line.substring(line.lastIndexOf(" ") + 1);
		
		String name = line.substring(line.indexOf("create"), line.indexOf("of"));
		name = name.replace("create", " ");
		
		p.println(category + name + " = new " + category + "();");
	}
	
	public static void ifer(String line)
	{
		line = "if " + "(" + line.substring(3)+ ")\n{"; 
		p.println(line);
	}

	public static void orifer(String line)
	{
		line = "else if " + "(" + line.substring(6) + ")\n{";
		p.println(line);
	}

	public static void orer()
	{
		p.println("else\n{");
	}
	
	public static void curlyBrace()
	{
		p.println("}\n");
	}
	
	public static void pass(String line)
	{
		p.println(line + ";");
	}
	
	public static void closePrintWriter()
	{
		p.close();
	}
	
	public static boolean isInteger(String s)
    {
    	try
    	{ 
        	Integer.parseInt(s); 
    	}
    	catch(NumberFormatException e)
    	{ 
    	    return false; 
    	}
    	// only got here if we didn't return false
    	return true;
	}
	
	public static boolean isDouble(String s)
	{
		if (s.contains("."))
		{
    		try
    		{ 
        		Double.parseDouble(s); 
    		}
    		catch(NumberFormatException e)
    		{ 
    		    return false; 
    		}
    		// only got here if we didn't return false
    		return true;
    	}
    	else
    		return false;
	}
}
