import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.tools.*;

public class Indigo2
{
	static String className = new Scanner(System.in).next();
	static IBasicFeatures ibf = new IBasicFeatures(className);
	
	public static void main(String[] args)
	{
		Indigo2 i2 = new Indigo2();

    	try
    	{
        	Scanner sc = new Scanner(new File(className + ".igo"));

        	while (sc.hasNextLine())
        	{
           		i2.processCode(sc.nextLine());
        	}
        	
        	sc.close();
    	} 
    	catch (FileNotFoundException e)
    	{
        	e.printStackTrace();
    	}
    	
    	ibf.closePrintWriter();
    	ibf.compileAndRun();
	}
	
	public void processCode(String code)
	{
		String line = removeSpaces(code.trim());
			
		if (line.startsWith("category "))
			ibf.writeClassHeader(line);
		else if (line.startsWith("task "))
			ibf.writeMethodHeader(line);
		else if (line.startsWith("print "))
			 ibf.print(line);
		else if (line.startsWith("line "))
			ibf.line(line);
		else if (line.startsWith("<indigo>"))
			ibf.mainMethodHeader();	
		else if (line.startsWith("let "))
			ibf.declareAndInitialize(line);
		else if (line.startsWith("do "))
			ibf.methodCall(line);
		else if (line.startsWith("Start:"))
			ibf.tryer(line);
		else if (line.startsWith("End:"))
			ibf.catcher(line);
		else if (line.startsWith("create "))
			ibf.objectCreation(line);
		else if (line.startsWith("."))
			ibf.curlyBrace();
		else if(line.startsWith("repeat "))
			ibf.forLoop(line);
		else if (line.startsWith("if "))
			ibf.ifer(line);
		else if (line.startsWith("or if "))
			ibf.orifer(line);
		else if (line.startsWith("or "))
			ibf.orer();
		else if(line.startsWith("drawImage") && line.contains(" to "))
			ibf.image3(line);
		else if (line.startsWith("drawImage") && line.contains(" and "))
			ibf.image2(line);
		else if(line.startsWith("drawImage"))
			ibf.image1(line);
		else if (!"".equals(line.trim()) && line.startsWith("*"))
			ibf.pass(line);
	}
	
	public String removeSpaces(String line)
	{
		String[] parts = line.split("(?=\")");

		for (int i = 0; i < parts.length; i++)
		{
			if (i == 0)
				parts[i] = parts[i].replaceAll("\\s+", " ");
			else if (parts[i].startsWith("\"") && !parts[i - 1].startsWith("\""));
				parts[i] = parts[i].replaceAll("\\s+", " ");
		}
		
		line = "";
		
		for (int i = 0; i < parts.length; i++)
			line += parts[i];
		
		return line;
	}
}
